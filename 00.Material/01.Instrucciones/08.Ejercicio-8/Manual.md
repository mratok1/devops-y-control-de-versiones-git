# Ejercicio 8

## El merge y el mergetool

Vale tenemos conflictos, el merge automático no funciona y debemos mergear manualmente.

### Lanzar el mergetool

Cuando existe algún conflicto, git nos lo informa en consola, nosotros simplemente tendremos que resolverlo con el mergetool que tengamos configurado y salvarlo

```
$ git mergetool
```

### Configurar el mergetool

Configuraremos MELD como herramienta de merge y resolución de conflictos. Puedes descargarla en cualquier momento mediante este link: [https://www.dropbox.com/s/u11yr82r648gkxl/mergeTool.zip?dl=0](https://www.dropbox.com/s/u11yr82r648gkxl/mergeTool.zip?dl=0)
Las instrucciones de instalación están dentro del zip.

### Mergear una rama manualmente contra otra

A veces queremos hacer un proceso de merge manual, no tiene que ver con traernos cambios con un fetch, simplemente por algunas razones queremos mergear una rama contra otra, para ello:

1. Vamos a la ramaA en la que quiero mergear los cambios de la ramaB

   ```
   $ git checkout ramaA
   ```
2. Una vez posicionados nos traemos los cambios de la ramaB para mergearlos con la rama en la que estamos posicionados, en este caso la ramaA

   ```
   $ git merge ramaB
   ```